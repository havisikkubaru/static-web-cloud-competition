import Link from "next/link";
import style from '../styles/Header.module.scss';

export default function Header() {
    return (
        <>
            <div className="container">
                <div className="col6">
                    <p className={style.logoTitle}><span className={style.cloud}>Cloud</span>Age</p>
                </div>
                <div className="col6">
                    <ul className={style.navLinks}>
                        <li>
                            <Link href="/"><a className={style.navItems}>Home</a></Link>
                        </li>
                        <li>
                            <Link href="/whyCloud"><a className={style.navItems}>Why Cloud</a></Link>
                        </li>
                        <li>
                            <Link href="/learn"><a className={style.navItems}>Learn</a></Link>
                        </li>
                    </ul>
                </div>
            </div>
        </>
    )
}